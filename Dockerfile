FROM gun27311/ubuntu-nodejs 
# image ที่จะใช้ ผมใช้อันที่พึ่งสร้างเลย ใครจะใช้อันอื่นก็ได้

WORKDIR /usr/src 
#คือ path ที่เราจะใช้ใน container

COPY . . 
#copy file ใน direatory ลงไปใน container

EXPOSE 3000 
#port ที่จะ map กับ host

RUN npm i
 # install dependency package

CMD npm start 